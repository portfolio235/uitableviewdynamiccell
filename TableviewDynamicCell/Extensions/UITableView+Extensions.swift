//
//  UITableView+Extensions.swift
//  TableviewDynamicCell
//
//  Created by Massimiliano Bonafede on 26/06/24.
//

import UIKit

extension UITableView: MainThreadDispatcherHelper {
    func scrollToBottom(_ animated: Bool = true) {
        let numberOfSections = self.numberOfSections
        guard numberOfSections > 0 else { return }
        let numberOfRows = self.numberOfRows(inSection: numberOfSections - 1)
        guard numberOfRows > 1 else { return }
        let indexPath = IndexPath(row: numberOfRows - 1, section: (numberOfSections - 1))
        dispatchMain { [weak self] in
            guard let self = self else { return }
            self.scrollToRow(at: indexPath, at: .bottom, animated: animated)
        }
    }
    
    func registerCell(_ cell: AnyClass) {
        let name = String(describing: cell)
        let bundle = Bundle(for: cell)
        let nib = UINib(nibName: name, bundle: bundle)
        register(nib, forCellReuseIdentifier: name)
    }
    
    func dequeueReusableCell<T: UITableViewCell>() -> T {
        let identifier = String(describing: T.self)
        return dequeueReusableCell(withIdentifier: identifier) as! T
    }
}
