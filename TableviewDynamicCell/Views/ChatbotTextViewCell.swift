//
//  ChatbotTextViewCell.swift
//  Multisistema
//
//  Created by Massimiliano Bonafede on 19/06/24.
//  Copyright © 2024 Urmet. All rights reserved.
//

import UIKit

protocol ChatbotTextViewCellDelegate: AnyObject {
    func didPressSend(_ text: String)
    func didPressSkip()
}

protocol HighlightDelegate: AnyObject {
    func didHighlight(isHighlighted: Bool)
}

class ChatbotTextViewCell: UITableViewCell {

    // MARK: - Outlets

    @IBOutlet weak var titleLabel: UITextView!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var hintLabel: UILabel!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var skipButton: UIButton!
    @IBOutlet weak var limitLabel: UILabel!
    @IBOutlet weak var canvasImageView: UIImageView!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet var topConstraint: NSLayoutConstraint!
    @IBOutlet var bottomConstraint: NSLayoutConstraint!
    
    // MARK: - Properties
    
    weak var delegate: ChatbotTextViewCellDelegate? = nil
    weak var highlightDelegate: HighlightDelegate? = nil
    private let limit: Int = 256
    
    // MARK: - Lifecycle

    override func awakeFromNib() {
        super.awakeFromNib()
        configureTextView()
        configureSendButton()
        configureSkipButton()
        configureHintLabel()
        configureLimitLabel()
        checkSendButtonIsEnabled(text: descriptionTextView.text)
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        descriptionTextView.text = nil
        hintLabel.text = nil
        limitLabel.text = "\(descriptionTextView.text.count)/256"
        checkSendButtonIsEnabled(text: descriptionTextView.text)
    }

    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        highlight(highlighted, animated: animated)
    }
    
    private func highlight(_ highlighted: Bool, animated: Bool) {
        UIView.animate(withDuration: 0.2) { [weak self] in
            guard let self = self else { return }
            highlightDelegate?.didHighlight(isHighlighted: highlighted)
        }
    }
    
    // MARK: - Methods

    private func configureTitleLabel() {
        titleLabel.isUserInteractionEnabled = true
        titleLabel.font = UIFont(name: "Copperplate-Bold", size: 17)!
    }
    
    private func configureTextView() {
        descriptionTextView.text = nil
        descriptionTextView.font = UIFont(name: "Copperplate-Bold", size: 14)!
        descriptionTextView.layer.cornerRadius = 5
        descriptionTextView.layer.borderColor = UIColor.black.cgColor
        descriptionTextView.layer.borderWidth = 1
        descriptionTextView.delegate = self
    }
    
    private func configureSendButton() {
        sendButton.setTitle("Confirm", for: .normal)
        sendButton.backgroundColor = UIColor.white
        sendButton.setTitleColor(UIColor.black, for: .normal)
        sendButton.titleLabel?.font = UIFont(name: "Copperplate-Bold", size: 17)!
        sendButton.titleLabel?.textAlignment = .center
    }
    
    private func configureSkipButton() {
        skipButton.setTitle("Skip", for: .normal)
        skipButton.backgroundColor = UIColor.white
        skipButton.setTitleColor(UIColor.black, for: .normal)
        skipButton.titleLabel?.font = UIFont(name: "Copperplate-Bold", size: 17)!
        skipButton.titleLabel?.textAlignment = .center
    }
    
    private func configureHintLabel() {
        hintLabel.text = nil
        hintLabel.textColor = .black
        hintLabel.font = UIFont(name: "Copperplate-Light", size: 14)!
    }
    
    private func configureLimitLabel() {
        limitLabel.text = "\(descriptionTextView.text.count)/256"
        limitLabel.textColor = .black
        limitLabel.font = UIFont(name: "Copperplate-Light", size: 14)!
    }
    
    private func checkSendButtonIsEnabled(text: String) {
        let range = 30 ... limit
        let count = text.count
        sendButton.isEnabled = range.contains(count)
    }
    
    func hideComponents() {
        stackView.isHidden = true
        hintLabel.isHidden = true
        limitLabel.isHidden = true
        skipButton.isHidden = true
        sendButton.isHidden = true
        topConstraint.isActive = false
        bottomConstraint.isActive = false
        
    }
    
    func restoreComponents() {
        stackView.isHidden = false
        topConstraint.isActive = true
        bottomConstraint.isActive = true
        hintLabel.isHidden = false
        limitLabel.isHidden = false
        skipButton.isHidden = false
        sendButton.isHidden = false
    }
    
    // MARK: - Actions

    @IBAction func sendButtonWasPressed(_ sender: UIButton) {
        descriptionTextView.resignFirstResponder()
        descriptionTextView.isUserInteractionEnabled = false
        delegate?.didPressSend(descriptionTextView.text)
    }
    
    @IBAction func skipButtonWasPressed(_ sender: UIButton) {
        descriptionTextView.resignFirstResponder()
        descriptionTextView.isUserInteractionEnabled = false
        delegate?.didPressSkip()
    }
}

// MARK: - UITextViewDelegate

extension ChatbotTextViewCell: UITextViewDelegate {
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        limitLabel.text = "\(textView.text.count)/256"
        checkSendButtonIsEnabled(text: textView.text)
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        limitLabel.text = "\(textView.text.count)/256"
        checkSendButtonIsEnabled(text: textView.text)
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let isInLimit = newText.count <= limit
        
        if isInLimit == false {
            textView.resignFirstResponder()
        }
        return isInLimit
    }
}
