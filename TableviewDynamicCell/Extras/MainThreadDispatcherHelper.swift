//
//  MainThreadDispatcherHelper.swift
//  TableviewDynamicCell
//
//  Created by Massimiliano Bonafede on 26/06/24.
//

import Foundation

public protocol MainThreadDispatcherHelper {
    func dispatchMain(_ completion: @escaping () -> Void)
    func dispatchMain(after interval: Double, _ completion: @escaping () -> Void)
}

extension MainThreadDispatcherHelper {
    public func dispatchMain(_ completion: @escaping () -> Void) {
        guard Thread.isMainThread else {
            return DispatchQueue.main.async(execute: completion)
        }
        
        completion()
    }
    
    public func dispatchMain(after interval: Double, _ completion: @escaping () -> Void) {
        DispatchQueue.main.asyncAfter(deadline: .now() + interval, execute: completion)
    }
}
