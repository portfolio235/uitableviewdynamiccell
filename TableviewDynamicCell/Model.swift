//
//  Model.swift
//  TableviewDynamicCell
//
//  Created by Massimiliano Bonafede on 27/06/24.
//

import UIKit

protocol ModelDelegate {
    func heightForRow() -> CGFloat
    func view(at tableview: UITableView) -> UITableViewCell
    func didEndDisplaying()
}

final class Model: ModelDelegate, ChatbotTextViewCellDelegate, HighlightDelegate {
    
    // MARK: - Properties

    private let title: String
    private let hint: String
    private var cell: ChatbotTextViewCell? = nil
    private var tableview: UITableView? = nil
    weak var delegate: ViewModelDelegate? = nil
    private var text: String? = nil
    private var shouldHideComponents: Bool = false {
        didSet {
            if shouldHideComponents {
                cell?.hideComponents()
            } else {
                cell?.restoreComponents()
            }
        }
    }
    
    // MARK: - Lifecycle

    init(title: String, hint: String) {
        self.title = title
        self.hint = hint
    }
    
    deinit {
        didEndDisplaying()
    }
    
    // MARK: - Methods

    func heightForRow() -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func view(at tableview: UITableView) -> UITableViewCell {
        cell = tableview.dequeueReusableCell()
        cell?.delegate = self
        cell?.highlightDelegate = self
        cell?.titleLabel.text = title
        cell?.hintLabel.text = hint
        cell?.canvasImageView.image = UIImage(resource: .intercomGeneric)
        cell?.descriptionTextView.isUserInteractionEnabled = !shouldHideComponents
        cell?.descriptionTextView.text = text
        shouldHideComponents ? cell?.hideComponents() : cell?.restoreComponents()
        return cell!
    }
    
    func didEndDisplaying() {
        cell = nil
        tableview = nil
    }
    
    func didPressSend(_ text: String) {
        self.text = text
        shouldHideComponents.toggle()
        delegate?.performBatchUpdates()
    }
    
    func didPressSkip() {
        shouldHideComponents.toggle()
        delegate?.performBatchUpdates()
    }
    
    func didHighlight(isHighlighted: Bool) {
        let image = UIImage(resource: .intercomGeneric)
        if isHighlighted {
            let color = UIColor(red: 42/255, green: 170/255, blue: 227/255, alpha: 1)
            cell?.canvasImageView.image = image.withTintColor(color)
        } else {
            cell?.canvasImageView.image = image
        }
    }
}
