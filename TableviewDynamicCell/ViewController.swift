//
//  ViewController.swift
//  TableviewDynamicCell
//
//  Created by Massimiliano Bonafede on 26/06/24.
//

import UIKit

class ViewController: UIViewController, MainThreadDispatcherHelper {

    // MARK: - Outlets

    @IBOutlet weak var tableview: UITableView!
    
    // MARK: - Properties

    let viewModel = ViewModel()
    
    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.delegate = self
        tableview.registerCell(ChatbotTextViewCell.self)
        tableview.dataSource = self
        tableview.delegate = self
    }
    
    // MARK: - Actions

    @IBAction func collectionViewButtonWasPressed(_ sender: UIButton) {
        dispatchMain { [weak self] in
            guard let self = self else { return }
            let bundle = Bundle(for: CollectionViewController.self)
            let controller = CollectionViewController(nibName: "CollectionViewController", bundle: bundle)
            let viewModel = CollectionViewModel()
            controller.viewModel = viewModel
            controller.modalPresentationStyle = .overCurrentContext
            present(controller, animated: true)
        }
    }
}

// MARK: - ViewModelDelegate

extension ViewController: ViewModelDelegate {
    func performBatchUpdates() {
        dispatchMain { [weak self] in
            guard let self = self else { return }
            tableview.performBatchUpdates {
            } completion: { [weak self] _ in
                guard let self = self else { return }
                viewModel.createModel()
                tableview.reloadData()
                tableview.scrollToBottom()
            }
        }
    }
}

// MARK: - UITableViewDelegate & UITableViewDataSource

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRowsInSection()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return viewModel.heightForRow(at: indexPath)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return viewModel.view(at: tableView, for: indexPath)
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        viewModel.didEndDisplaying(for: indexPath)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
