//
//  ViewModel.swift
//  TableviewDynamicCell
//
//  Created by Massimiliano Bonafede on 27/06/24.
//

import UIKit

protocol ViewModelDelegate: AnyObject {
    func performBatchUpdates()
}

final class ViewModel {
    
    // MARK: - Properties

    weak var delegate: ViewModelDelegate? = nil
    private var models: [ModelDelegate] = []
    private(set) var counter: Int = 0
    
    // MARK: - Lifecycle

    init() {
        createModel()
    }
    
    // MARK: - Methods

    func createModel() {
        let model: Model
        if Bool.random() {
            model = Model(title: "This is a multiline description to observe the changing on the layout, using to calculate the correct constraints for the label. for model \(counter)", hint: "At least 30 characters")
        } else {
            model = Model(title: "Insert a description for model \(counter)", hint: "At least 30 characters")
        }
        
        model.delegate = self
        models.append(model)
        counter += 1
    }
    
    func heightForRow(at indexPath: IndexPath) -> CGFloat {
        return models[indexPath.row].heightForRow()
    }
    
    func numberOfRowsInSection() -> Int {
        return models.count
    }
    
    func view(at tableview: UITableView, for indexPath: IndexPath) -> UITableViewCell {
        return models[indexPath.row].view(at: tableview)
    }
    
    func didEndDisplaying(for indexPath: IndexPath) {
        models[indexPath.row].didEndDisplaying()
    }
}

extension ViewModel: ViewModelDelegate {
    func performBatchUpdates() {
        delegate?.performBatchUpdates()
    }
}
